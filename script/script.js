//1. setTimeout() виконується лише 1 раз, setInterval() запускається багаторазово, повторюючись поки не створити умовуБ щоб зупини функцію
//2. Якщо в функцію setTimeout() передати нульову затримку, вона запуститься одразу, але після завершення поточного скрипту
//3. Тому що, clearInterval() відміняє дію функцій setTimeout() і setInterval()

let img = document.querySelectorAll('.image-to-show');
let imgArr = Array.from(img);

let btnStart = document.getElementById('start');
let btnStop = document.getElementById('stop');

let current = 0;
let previous = 0;

function showImages(){
    setTimeout(() => {
       btnStart.style.display = 'block';
       btnStop.style.display = 'block';
    }, 2000);
    
    imgArr[previous].classList.remove('show');
    current++;

    if(current > imgArr.length - 1){
        current = 0;
    }

    imgArr[current].classList.add('show');
    previous = current;
}

let imageShow = setInterval(showImages, 3000);

let showing = true;

function pause(){
    showing = false;
    clearInterval(imageShow);
}

function play(){
    showing = true;
    imageShow = setInterval(showImages, 3000);
}

btnStop.addEventListener('click', pause);


btnStart.addEventListener('click', play);
